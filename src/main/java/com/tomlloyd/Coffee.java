package com.tomlloyd;

import java.util.List;

public interface Coffee {

    String getName();

    List<String> getIngredients();
}
