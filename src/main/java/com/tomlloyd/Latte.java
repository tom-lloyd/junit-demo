package com.tomlloyd;

import java.util.Arrays;
import java.util.List;

public class Latte implements Coffee {

    @Override
    public String getName() {
        return "latte";
    }

    @Override
    public List<String> getIngredients() {
        return Arrays.asList("espresso", "milk");
    }
}
