package com.tomlloyd;

public class NoSuchCoffeeException extends RuntimeException {

    public NoSuchCoffeeException(String message) {
        super(message);
    }
}
