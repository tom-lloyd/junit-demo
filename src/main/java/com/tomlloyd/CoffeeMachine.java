package com.tomlloyd;

import java.util.HashMap;
import java.util.Map;

public class CoffeeMachine {

    private static final Map<String, Coffee> coffeeMenu = new HashMap<>();

    static {
        coffeeMenu.put("latte", new Latte());
    }

    public Coffee make(String coffeeName) {
        if (coffeeName != null && coffeeMenu.containsKey(coffeeName)) {
            return coffeeMenu.get(coffeeName);
        }

        throw new NoSuchCoffeeException(String.format("Coffee with name '%s' is not on the menu!", coffeeName));
    }
}
