package com.tomlloyd;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class CoffeeMachineTest {

    private CoffeeMachine coffeeMachine;

    @Before
    public void setUp() {
        coffeeMachine = new CoffeeMachine();
    }

    @Test
    public void make_validCoffee_returnsCoffee() {
        // GIVEN
        final String testCoffeeName = "latte";

        // WHEN
        final Coffee coffee = coffeeMachine.make(testCoffeeName);

        // THEN
        assertThat(coffee.getName(), is(testCoffeeName));
        assertThat(coffee, is(instanceOf(Latte.class)));
        assertThat(coffee.getIngredients(), hasItem("milk"));
    }

    @Test(expected = NoSuchCoffeeException.class)
    public void make_invalidCoffee_throwsException () {
        // GIVEN
        final String testCoffeeName = "cheese";

        // WHEN
        coffeeMachine.make(testCoffeeName);
    }
}